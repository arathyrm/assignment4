package Testbase;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

public class Tests {
	WebDriver driver;
	//tc01
	//launch application
	@Given("^the url http://spezicoe\\.wipro\\.com:(\\d+)/opencart(\\d+)/ is launched$")
	public void the_url_http_spezicoe_wipro_com_opencart_is_launched(int arg1, int arg2) throws Throwable {
	    
        System.setProperty("webdriver.chrome.driver", "D:\\Drivers\\chromedriver.exe");
		  driver=new ChromeDriver(); 
		  driver.get("https://demo.opencart.com");      
	}
	//Click on 'my Account' and 'Register'
	@When("^the user clicks on My Account and Register buttons$")
	public void the_user_clicks_on_My_Account_and_Register_buttons() throws Throwable {
	   
		driver.findElement(By.linkText("My Account")).click();
	     driver.findElement(By.linkText("Register")).click();
	     assertEquals("https://demo.opencart.com/index.php?route=account/register",driver.getCurrentUrl());
	}
	//Enter the mandatory fields
	@When("^the user enters the mandatory fields$")
	public void the_user_enters_the_mandatory_fields() throws Throwable {
	   
		 File f=new File("D:\\20111748\\Assign4\\registraion.xls");
         Workbook b=Workbook.getWorkbook(f);
       Sheet s1=b.getSheet(0);
       int col=s1.getColumns();
       String []s=new String[col];
       for(int i=0;i<col;i++){
           Cell c=s1.getCell(i, 0);
           s[i]=c.getContents();
           System.out.println(s[i]);
       }
       driver.findElement(By.name("firstname")).sendKeys(s[0]);
       driver.findElement(By.name("lastname")).sendKeys(s[1]);
       driver.findElement(By.name("email")).sendKeys(s[2]);
       driver.findElement(By.name("telephone")).sendKeys(s[3]);
       driver.findElement(By.name("password")).sendKeys(s[4]);
       driver.findElement(By.name("confirm")).sendKeys(s[5]);
	    
	}
	//checks the privacy policy checkbox
	@When("^checks the privacy policy checkbox$")
	public void checks_the_privacy_policy_checkbox() throws Throwable {
	    
		 driver.findElement(By.xpath("//input[@type='checkbox']")).click();
		 assertTrue(driver.findElement(By.xpath("//input[@type='checkbox']")).isEnabled()); 
	}
	//Click on continue button
	@When("^clicks the continue button$")
	public void clicks_the_continue_button() throws Throwable {
	   
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		 //assertEquals("https://demo.opencart.com/index.php?route=account/success",driver.getCurrentUrl());
		
	}
	//message display
	@Then("^Congratulations! Your new account has been successfully created! message should be displayed$")
	public void congratulations_Your_new_account_has_been_successfully_created_message_should_be_displayed() throws Throwable {
	   
		String s=driver.findElement(By.xpath("//div[@id=\"content\"]/p[1]")).getText();
		System.out.print(s);
		assertEquals("https://demo.opencart.com/index.php?route=account/success",driver.getCurrentUrl());
	  
	}
	//logout
	@Then("^User should be able to logout$")
	public void user_should_be_able_to_logout() throws Throwable {
	    
		driver.findElement(By.linkText("My Account")).click();
		driver.findElement(By.linkText("Logout")).click();
		assertEquals("https://demo.opencart.com/index.php?route=account/logout",driver.getCurrentUrl());

	}
	//tc02
	//login
	@When("^the user clicks on My Account and Login$")
	public void the_user_clicks_on_My_Account_and_Login() throws Throwable {
	   
		driver.findElement(By.linkText("My Account")).click();
		 driver.findElement(By.linkText("Login")).click();
	}
	//Enter email and password
	@When("^the user enters the Email and password$")
	public void the_user_enters_the_Email_and_password() throws Throwable {
	   
		 File f=new File("D:\\login.xls");
         Workbook b=Workbook.getWorkbook(f);
       Sheet s2=b.getSheet(0);
       int col=s2.getColumns();
       String []s=new String[col];
       for(int i=0;i<col;i++){
           Cell c=s2.getCell(i, 0);
           s[i]=c.getContents();
           System.out.println(s[i]);
       }
       driver.findElement(By.name("email")).sendKeys(s[0]);
       driver.findElement(By.name("password")).sendKeys(s[1]);
      
	}
	//click on login
	@When("^clicks the Login button$")
	public void clicks_the_Login_button() throws Throwable {
	   
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		 assertEquals("https://demo.opencart.com/index.php?route=account/account",driver.getCurrentUrl());
	}
	//Click on edit account link
	@When("^clicks the Edit account link$")
	public void clicks_the_Edit_account_link() throws Throwable {
	   
		driver.findElement(By.linkText("Edit your account information")).click();
		assertEquals("https://demo.opencart.com/index.php?route=account/edit",driver.getCurrentUrl());
	}
	//Enter the new telephone number
	@When("^enters the new telephone number$")
	public void enters_the_new_telephone_number() throws Throwable {
	   
		 driver.findElement(By.name("telephone")).clear();
		 String[] str= {"34567"};
		 driver.findElement(By.name("telephone")).sendKeys(str);
	}
	//Click on the continue button
	@Then("^clicks the second continue button")
	public void clicks_the_second_continue_button () throws Throwable {
		 driver.findElement(By.xpath("//input[@type='submit']")).click();
		 assertEquals("https://demo.opencart.com/index.php?route=account/account",driver.getCurrentUrl());
	}
	//message displayed
	@Then("^Success: Your account has been successfully updated! should be displayed$")
	public void success_Your_account_has_been_successfully_updated_should_be_displayed() throws Throwable {
	   
		 String r=driver.findElement(By.xpath("//div[@class=\"alert alert-success alert-dismissible\"]")).getText();
			System.out.print(r);
			assertEquals("https://demo.opencart.com/index.php?route=account/account",driver.getCurrentUrl());	
	}
	//tc04
	//counts the number of links
	@When("^counts the number of menu links$")
	public void counts_the_number_of_menu_links() throws Throwable {
	   
		WebElement nav=driver.findElement(By.className("navbar-nav"));
		List<WebElement> navs=nav.findElements(By.tagName("a"));
		int cnt=0;
		for(WebElement ele:navs) {
			if(!ele.getText().isEmpty())
			{
				
				cnt++;
			}
	}
		
		System.out.print("count="+cnt);
	}
	//click on each menu link
	@Then("^clicks on each menu link (\\d+) by (\\d+) and takes snapshot$")
	public void clicks_on_each_menu_link_by_and_takes_snapshot(int arg1, int arg2) throws Throwable {
	   
		driver.findElement(By.linkText("Desktops")).click();
		 TakesScreenshot scrShot =((TakesScreenshot)driver);
	     File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
	     File link1=new File("D:\\20111748\\Assign4\\Snapshots\\link1.png");
	     FileUtils.copyFile(SrcFile,link1);
	     
		driver.findElement(By.linkText("Laptops & Notebooks")).click();
		 TakesScreenshot scrShot2 =((TakesScreenshot)driver);
	     File SrcFile2=scrShot2.getScreenshotAs(OutputType.FILE);
	     File link2=new File("D:\\20111748\\Assign4\\Snapshots\\link2.png");
	     FileUtils.copyFile(SrcFile2, link2);
	     
	     driver.findElement(By.linkText("Components")).click();
		 TakesScreenshot scrShot3 =((TakesScreenshot)driver);
	     File SrcFile3=scrShot3.getScreenshotAs(OutputType.FILE);
	     File link3=new File("D:\\20111748\\Assign4\\Snapshots\\link3.png");
	     FileUtils.copyFile(SrcFile3, link3);
	     
	     driver.findElement(By.linkText("Tablets")).click();
		 TakesScreenshot scrShot4 =((TakesScreenshot)driver);
	     File SrcFile4=scrShot4.getScreenshotAs(OutputType.FILE);
	     File link4=new File("D:\\20111748\\Assign4\\Snapshots\\link4.png");
	     FileUtils.copyFile(SrcFile4, link4);
	     
	     driver.findElement(By.linkText("Software")).click();
		 TakesScreenshot scrShot5 =((TakesScreenshot)driver);
	     File SrcFile5=scrShot5.getScreenshotAs(OutputType.FILE);
	     File link5=new File("D:\\20111748\\Assign4\\Snapshots\\link5.png");
	     FileUtils.copyFile(SrcFile5, link5);
	     
	     driver.findElement(By.linkText("Phones & PDAs")).click();
		 TakesScreenshot scrShot6 =((TakesScreenshot)driver);
	     File SrcFile6=scrShot6.getScreenshotAs(OutputType.FILE);
	     File link6=new File("D:\\20111748\\Assign4\\Snapshots\\link6.png");
	     FileUtils.copyFile(SrcFile6, link6);
	     
	     driver.findElement(By.linkText("Cameras")).click();
		 TakesScreenshot scrShot7 =((TakesScreenshot)driver);
	     File SrcFile7=scrShot7.getScreenshotAs(OutputType.FILE);
	     File link7=new File("D:\\20111748\\Assign4\\Snapshots\\link7.png");
	     FileUtils.copyFile(SrcFile7,link7);
	     
	     driver.findElement(By.linkText("MP3 Players")).click();
		 TakesScreenshot scrShot8 =((TakesScreenshot)driver);
	     File SrcFile8=scrShot8.getScreenshotAs(OutputType.FILE);
	     File link8=new File("D:\\20111748\\Assign4\\Snapshots\\link8.png");
	     FileUtils.copyFile(SrcFile8, link8);
	     
	}
	//click on the store link
	@When("^clicks on store link available on top left corner$")
	public void clicks_on_store_link_available_on_top_left_corner() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.linkText("Your Store")).click();
		assertEquals("https://demo.opencart.com/index.php?route=common/home",driver.getCurrentUrl());
	}
	//home page displayed
	@Then("^Home page should be displayed$")
	public void home_page_should_be_displayed() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	  assertEquals("https://demo.opencart.com/index.php?route=common/home",driver.getCurrentUrl()); 
	}
	//Click on the brand links available
	@Then("^clicks on Brands link available on bottom navbar under Extras$")
	public void clicks_on_Brands_link_available_on_bottom_navbar_under_Extras() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.linkText("Brands")).click(); 

	}
//find favourite brand page
	@Then("^Find your favourite Brand page should be displayed$")
	public void find_your_favourite_Brand_page_should_be_displayed() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		assertEquals("https://demo.opencart.com/index.php?route=product/manufacturer",driver.getCurrentUrl()); 
	}


	
	
}




